﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using interview.exercises.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace interview.exercises.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Temperature> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 3).Select(index => new Temperature(DateTime.Now.AddDays(index),rng.NextDouble())).ToArray();
        }
    }
}
