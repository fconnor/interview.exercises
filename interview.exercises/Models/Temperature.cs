﻿using System;

namespace interview.exercises.Models
{
    public class Temperature
    {
        public DateTime Time { get; private set; }
        public double Value { get; private set; }

        public Temperature(DateTime time, double temperature)
        {
            Time = time;
            Value = Math.Round(temperature, 1);
        }

    }
}
