﻿# Purpose

This project is intended as a paired exercise to walk through as an assesment of how a candidate works on a problem. 
The flow below offers a suggestion of how to progress through but it is not necessary to follow this or complete all activies. 
At any time, any part of the code base may be altered. 
Google can be used to search for solutions to issues. 

# Suggested Flow

1. The unit test in WeatherForecastControllerTests does nothing, start by fixing this so that it tests functionality. 
2. The weatherForecastController should probably return IActionResults. 
3. Add a POST method to store data in some for of persistence. 
4. Dockerise the application. 
5. Should a candidate get this far, any other work can be considered. E.g. Memory Caches, extending out the REST functions, Expanding the persistence.....
