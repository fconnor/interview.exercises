using Castle.Core.Logging;
using interview.exercises.Controllers;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace interview.exercises.test
{
    [TestClass]
    public class WeatherForecastControllerTests
    {
        [TestMethod]
        [Description("This tests nothing useful")]
        public void WeatherForcecastController_Get_ReturnsValues()
        {
            var mockLogger = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(mockLogger.Object);

            var resultValue = controller.Get();

            Assert.AreEqual(3, resultValue.Count());
        }
    }
}
